package org.hy.automate.utils;

import org.hy.automate.annotation.Table;
import org.hy.automate.handler.FieldConfig;

import java.lang.reflect.Field;
import java.util.List;

public class BeanUtils {
    public static String getTableName(Class<?> beanClass) {
        Table table = beanClass.getAnnotation(Table.class);
        String tableName = "";
        if(table!=null){
            tableName = table.value();
            if(StringUtils.isEmpty(tableName)){
                tableName = table.tableName();
            }
        }
        if(StringUtils.isEmpty(tableName)){
            String beanName = beanClass.getSimpleName();
            char[] cs = beanName.toCharArray();
            StringBuilder builder = new StringBuilder();
            int i=0;
            for(char c:cs){
                if(Character.isUpperCase(c)){
                    if(i!=0){
                        builder.append("_");
                    }
                }
                i++;
                builder.append(Character.toLowerCase(c));
            }
            tableName = builder.toString();
        }
        return tableName;
    }

    public static String[] getNameList(List<FieldConfig> fieldConfigs,boolean fieldName) {
        String[] fieldNames = new String[fieldConfigs.size()];
        int i = 0;
        for (FieldConfig fieldConfig:fieldConfigs) {
            if(fieldName){
                fieldNames[i++] = fieldConfig.getJavaProperty();
            }else {
                fieldNames[i++] = fieldConfig.getColumn();
            }
        }
        return fieldNames;
    }

    public static String getColumnName(String fieldName) {
        char[] cs = fieldName.toCharArray();
        StringBuilder columnNameBuilder = new StringBuilder();
        for (char c:cs){
            if(Character.isUpperCase(c)){
              columnNameBuilder.append("_").append(Character.toLowerCase(c));
            }else {
                columnNameBuilder.append(c);
            }
        }
        return columnNameBuilder.toString();
    }
}

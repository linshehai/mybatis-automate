package org.hy.automate.utils;

public class StringUtils {

    public static boolean isEmpty(String str){
        return str==null||str.isEmpty();
    }

    public static String getDefaultString(String target,String defaultStr){
        return isEmpty(target)?defaultStr:target;
    }
}

package org.hy.automate.exception;

public class UnknownDataBaseTypeException extends RuntimeException {
    public UnknownDataBaseTypeException(String message) {
        super(message);
    }
}

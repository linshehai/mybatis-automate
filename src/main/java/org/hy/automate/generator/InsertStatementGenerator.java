package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.StaticTextSqlNode;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.handler.ModelDefinition;

import java.util.ArrayList;
import java.util.List;

public class InsertStatementGenerator extends AbstractGenerator{
    public InsertStatementGenerator() {
        super("insert");
    }

    @Override
    public MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("insert into ").append(modelDefinition.getTableName()).append(" (");
        sqlBuilder.append(modelDefinition.getColumnList()).append(")");
        String values = String.join("},#{",modelDefinition.getFieldNames());
        String sql = sqlBuilder.append("values(#{").append(values).append("})").toString();
        List<SqlNode> contents = new ArrayList<>();
        contents.add(new StaticTextSqlNode(sql));
        return new MixedSqlNode(contents);
    }
    @Override
    public MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode sqlNode = getSqlNodes(configuration, mapperClass, modelDefinition);
        if(sqlNode!=null){
            String namespace = mapperClass.getName();
            String statementId = namespace+"."+this.method;
            return register(configuration,sqlNode, SqlCommandType.INSERT,statementId,namespace,Integer.class,null);
        }
        return null;
    }
}

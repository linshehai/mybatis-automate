package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.scripting.xmltags.IfSqlNode;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SetSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;
import org.apache.ibatis.scripting.xmltags.WhereSqlNode;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.handler.FieldConfig;
import org.hy.automate.handler.ModelDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UpdateByIdStatementGenerator extends AbstractGenerator{
    public UpdateByIdStatementGenerator() {
        super("updateByIdSelective");
    }

    @Override
    protected MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        List<FieldConfig> configs = modelDefinition.getFieldConfigs();
        List<FieldConfig> fieldConfigs = configs.stream().
                filter(fieldConfig -> fieldConfig.isPrimaryKey()).collect(Collectors.toList());
        if(fieldConfigs.size()>0){
            String tableName = modelDefinition.getTableName();
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append("update ").append(tableName);

            FieldConfig fieldConfig = fieldConfigs.get(0);
            TextSqlNode mainSql = new TextSqlNode(sqlBuilder.toString());
            String primaryKeyColumn = fieldConfig.getColumn();
            String primaryKeyField = fieldConfig.getJavaProperty();
            List<SqlNode> setNodes = new ArrayList<>();
            for (FieldConfig config : configs) {
                String columnName = config.getColumn();
                String property = config.getJavaProperty();
                String test = property+"!=null and "+property+" !='' ";
                String setPart = columnName+"=#{"+property+"},";
                TextSqlNode textSqlNode = new TextSqlNode(setPart);
                IfSqlNode setIfSqlNode = new IfSqlNode(textSqlNode,test);
                setNodes.add(setIfSqlNode);
            }
            SetSqlNode setSqlNode = new SetSqlNode(configuration,new MixedSqlNode(setNodes));
            TextSqlNode idConditionNode = new TextSqlNode( primaryKeyColumn+"=#{"+primaryKeyField+"}");
            WhereSqlNode where = new WhereSqlNode(configuration,idConditionNode);
            List<SqlNode> contents = new ArrayList<>();
            contents.add(mainSql);
            contents.add(setSqlNode);
            contents.add(where);
            return new MixedSqlNode(contents);
        }else{
            throw new IllegalStateException("no primary key was specified");
        }
    }
    @Override
    public MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode sqlNode = getSqlNodes(configuration, mapperClass, modelDefinition);
        if(sqlNode!=null){
            String namespace = mapperClass.getName();
            String statementId = namespace+"."+this.method;
            return register(configuration,sqlNode, SqlCommandType.UPDATE,statementId,namespace,Integer.class,null);
        }
        return null;
    }
}

package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.scripting.xmltags.ForEachSqlNode;
import org.apache.ibatis.scripting.xmltags.IfSqlNode;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;
import org.apache.ibatis.scripting.xmltags.WhereSqlNode;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.handler.ModelDefinition;

import java.util.ArrayList;
import java.util.List;

public class SelectByConditionStatementGenerator extends AbstractGenerator{

    public SelectByConditionStatementGenerator(String method) {
        super(method);
    }
    public SelectByConditionStatementGenerator() {
        super("select");
    }

    @Override
    protected MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {

        List<SqlNode> nodes = new ArrayList<>();
        SqlNode predNode = new TextSqlNode(" ${it.predict} ");
        SqlNode expSqlNode = new TextSqlNode(" ${expression.leftValue} ${expression.operator} #{expression.rightValue}");
        List<SqlNode> mxn = new ArrayList<>();
        ForEachSqlNode expressionsSqlNode = new ForEachSqlNode(configuration,expSqlNode,"it.expressions",null,"expression","(",")","it.predict");
        mxn.add(predNode);
        mxn.add(expressionsSqlNode);

        ForEachSqlNode forEachSqlNode = new ForEachSqlNode(configuration,new MixedSqlNode(mxn),"criteria.conjunctions",null,"it",null,null,null);
        String test="criteria!=null and criteria.conjunctions!=null";
        IfSqlNode ifSqlNode = new IfSqlNode(forEachSqlNode,test);

        String fieldList = modelDefinition.getColumnList();
        String tableName = modelDefinition.getTableName();
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT ").append(fieldList).append(" FROM ").append(tableName).append(" ");
        SqlNode selectSqlNode = new TextSqlNode(sqlBuilder.toString());
        WhereSqlNode whereSqlNode = new WhereSqlNode(configuration,ifSqlNode);
        nodes.add(selectSqlNode);
        nodes.add(whereSqlNode);
        return new MixedSqlNode(nodes);
    }
    @Override
    public MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode sqlNode = getSqlNodes(configuration, mapperClass, modelDefinition);
        if(sqlNode!=null){
            String namespace = mapperClass.getName();
            String statementId = namespace+"."+this.method;
            String resultMapName = namespace+"-BaseResultMap";
            return register(configuration,sqlNode, SqlCommandType.SELECT,statementId,namespace,modelDefinition.getModelClass(),resultMapName);
        }
        return null;
    }
}

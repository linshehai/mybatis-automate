package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.StaticTextSqlNode;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.handler.FieldConfig;
import org.hy.automate.handler.ModelDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DeleteByIdStatementGenerator extends AbstractGenerator{
    public DeleteByIdStatementGenerator() {
        super("deleteById");
    }

    @Override
    protected MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        List<FieldConfig> fieldConfigs = modelDefinition.getFieldConfigs().stream().
                filter(fieldConfig -> fieldConfig.isPrimaryKey()).collect(Collectors.toList());
        if(fieldConfigs.size()>0){
            String tableName = modelDefinition.getTableName();
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append("delete from ").append(tableName);
            FieldConfig fieldConfig = fieldConfigs.get(0);
            String fieldName = fieldConfig.getJavaProperty();
            sqlBuilder.append(" where ").append(fieldConfig.getColumn());
            sqlBuilder.append(" = #{").append(fieldName).append("}");
            String sql = sqlBuilder.toString();
            List<SqlNode> contents = new ArrayList<>();
            contents.add(new StaticTextSqlNode(sql));
            return new MixedSqlNode(contents);
        }else{
            throw new IllegalStateException("no primary key was specified");
        }
    }

    @Override
    public MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode sqlNode = getSqlNodes(configuration, mapperClass, modelDefinition);
        if(sqlNode!=null){
            String namespace = mapperClass.getName();
            String statementId = namespace+"."+this.method;
            return register(configuration,sqlNode, SqlCommandType.DELETE,statementId,namespace,Integer.class,null);
        }
        return null;
    }
}

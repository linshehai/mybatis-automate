package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.base.Page;
import org.hy.automate.plugin.PageInterceptor;
import org.hy.automate.dialect.Dialect;
import org.hy.automate.exception.UnknownDataBaseTypeException;
import org.hy.automate.handler.ModelDefinition;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PageQueryStatementGenerator extends SelectByConditionStatementGenerator{


    private Dialect dialect;

    private String[] urlMethodNames = {"getUrl","getJdbcUrl"};

    public PageQueryStatementGenerator() {
        super("selectPage");
    }

    @Override
    protected MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode selectSqlNode = super.getSqlNodes(configuration, mapperClass, modelDefinition);
        List<SqlNode> contents = new ArrayList<>();
        contents.add(selectSqlNode);
        if(dialect!=null){
            SqlNode limitSql = dialect.getLimitSql(configuration);
            contents.add(limitSql);
        }else {
                for (String urlMethodName : urlMethodNames) {
                    try {
                        Method urlMethod = configuration.getEnvironment().getDataSource().getClass().getMethod(urlMethodName);
                        String url = (String) urlMethod.invoke(configuration.getEnvironment().getDataSource(), new Object[]{});
                        String[] strs = url.split(":");
                        String databaseType = strs[1];
                        dialect = DIALECT_MAP.get(databaseType);
                        SqlNode limitSql = dialect.getLimitSql(configuration);
                        contents.add(limitSql);

                        break;
                    } catch (Exception e) {
                        //
                    }
                }

            if (dialect==null){
                throw new UnknownDataBaseTypeException("无法自动决定数据库类型");
            }
            List<Interceptor> interceptors = configuration.getInterceptors().stream()
                    .filter(interceptor -> interceptor instanceof PageInterceptor)
                    .collect(Collectors.toList());
            if(interceptors.size()<=0){
                configuration.addInterceptor(new PageInterceptor());
            }
        }
        return new MixedSqlNode(contents);
    }

    @Override
    public MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode sqlNode = getSqlNodes(configuration, mapperClass, modelDefinition);
        if(sqlNode!=null){
            String namespace = mapperClass.getName();
            String statementId = namespace+"."+this.method;
            String resultMapName = namespace+"-BaseResultMap";

            return register(configuration,sqlNode, SqlCommandType.SELECT,statementId,namespace, Page.class,resultMapName);
        }
        return null;
    }


}

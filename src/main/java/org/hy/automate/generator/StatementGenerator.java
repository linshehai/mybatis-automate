package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.handler.ModelDefinition;

public interface StatementGenerator {
    MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition);

    String getMethod();

}

package org.hy.automate.generator;

import org.apache.ibatis.builder.IncompleteElementException;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.scripting.xmltags.DynamicSqlSource;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.XMLLanguageDriver;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.dialect.Dialect;
import org.hy.automate.dialect.Mysql8Dialect;
import org.hy.automate.dialect.PostgresDialect;
import org.hy.automate.handler.ModelDefinition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractGenerator implements StatementGenerator{

    protected static Map<String, Dialect> DIALECT_MAP = new HashMap<>();
    static {
        DIALECT_MAP.put("mysql",new Mysql8Dialect());
        DIALECT_MAP.put("postgresql",new PostgresDialect());
    }
    protected String method;
    public AbstractGenerator(String method){
        this.method = method;
    }

    protected AbstractGenerator() {
    }

    protected abstract MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition);

    protected MappedStatement register(Configuration configuration, SqlNode rootSqlNode,
                                       SqlCommandType sqlCommandType,
                                       String statementId,
                                       String namespace,
                                       Class<?> returnType,
                                       String resultMapName){
        SqlSource sqlSource = new DynamicSqlSource(configuration, rootSqlNode);

        LanguageDriver languageDriver = configuration.getLanguageDriver(XMLLanguageDriver.class);
        MappedStatement.Builder statementBuilder = new MappedStatement.Builder(configuration,statementId,sqlSource,sqlCommandType);
        MappedStatement statement = statementBuilder.resource(null)
                .fetchSize(null)
                .timeout(null)
                .keyGenerator(new NoKeyGenerator())
                .keyProperty(null)
                .keyColumn(null)
                .databaseId(null)
                .lang(languageDriver)
                .resultOrdered(false)
                .resultSets(null)
                .resultMaps(getStatementResultMaps(namespace,configuration,resultMapName,returnType,statementId))
                .resultSetType(null)
                .flushCacheRequired(false)
                .useCache(true)
                .cache(null).build();
        configuration.addMappedStatement(statement);
        return statement;
    }


    private List<ResultMap> getStatementResultMaps(String namespace, Configuration configuration,
                                                          String resultMap,
                                                          Class<?> resultType,
                                                          String statementId) {
//        resultMap = namespace+"."+resultMap;

        List<ResultMap> resultMaps = new ArrayList<>();
        if (resultMap != null) {
            String[] resultMapNames = resultMap.split(",");
            for (String resultMapName : resultMapNames) {
                try {
                    resultMaps.add(configuration.getResultMap(resultMapName.trim()));
                } catch (IllegalArgumentException e) {
                    throw new IncompleteElementException("Could not find result map '" + resultMapName + "' referenced from '" + statementId + "'", e);
                }
            }
        } else if (resultType != null) {
            ResultMap inlineResultMap = new ResultMap.Builder(
                    configuration,
                    statementId + "-Inline",
                    resultType,
                    new ArrayList<>(),
                    null).build();
            resultMaps.add(inlineResultMap);
        }
        return resultMaps;
    }

    public String getMethod() {
        return method;
    }
}

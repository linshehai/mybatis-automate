package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.base.Page;
import org.hy.automate.handler.ModelDefinition;

import java.util.ArrayList;
import java.util.List;

public class CountStatementGenerator extends SelectByConditionStatementGenerator{


    public CountStatementGenerator() {
        super("count");
    }

    @Override
    protected MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        TextSqlNode countNode = new TextSqlNode("SELECT count(*) FROM( ");
        MixedSqlNode selectSqlNode = super.getSqlNodes(configuration, mapperClass, modelDefinition);
        TextSqlNode textSqlNode = new TextSqlNode(" ) CountStatementGenerator ");

        //todo 获取dialect的key
        /*Dialect dialect = DIALECT_MAP.get(null);
        String sql = dialect.getLimitSql(configuration);
        */
        List<SqlNode> contents = new ArrayList<>();
        contents.add(countNode);
        contents.add(selectSqlNode);
        contents.add(textSqlNode);
        return new MixedSqlNode(contents);
    }

    @Override
    public MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode sqlNode = getSqlNodes(configuration, mapperClass, modelDefinition);
        if(sqlNode!=null){
            String namespace = mapperClass.getName();
            String statementId = namespace+"."+this.method;
            String resultMapName = namespace+"-BaseResultMap";

            return register(configuration,sqlNode, SqlCommandType.SELECT,statementId,namespace, Long.class,null);
        }
        return null;
    }


}

package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.StaticTextSqlNode;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.handler.FieldConfig;
import org.hy.automate.handler.ModelDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SelectByIdStatementGenerator extends AbstractGenerator{
    public SelectByIdStatementGenerator() {
        super("selectById");
    }

    @Override
    public MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        List<FieldConfig> fieldConfigs = modelDefinition.getFieldConfigs().stream().
                filter(fieldConfig -> fieldConfig.isPrimaryKey()).collect(Collectors.toList());
        if(fieldConfigs.size()>0){
            String fieldList = modelDefinition.getColumnList();
            String tableName = modelDefinition.getTableName();
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append("SELECT ").append(fieldList).append(" FROM ").append(tableName);
            FieldConfig fieldConfig = fieldConfigs.get(0);
            String fieldName = fieldConfig.getJavaProperty();
            sqlBuilder.append(" WHERE ").append(fieldConfig.getColumn());
            sqlBuilder.append(" = #{").append(fieldName).append("}");
            String sql = sqlBuilder.toString();
            List<SqlNode> contents = new ArrayList<>();
            contents.add(new StaticTextSqlNode(sql));
            return new MixedSqlNode(contents);
        }
        return null;
    }
    @Override
    public MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode sqlNode = getSqlNodes(configuration, mapperClass, modelDefinition);
        if(sqlNode!=null){
            String namespace = mapperClass.getName();
            String statementId = namespace+"."+this.method;
            String resultMapName = namespace+"-BaseResultMap";
            return register(configuration,sqlNode, SqlCommandType.SELECT,statementId,namespace,modelDefinition.getModelClass(),resultMapName);
        }
        return null;
    }
}

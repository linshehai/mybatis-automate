package org.hy.automate.generator;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;
import org.apache.ibatis.scripting.xmltags.WhereSqlNode;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.annotation.OneToMany;
import org.hy.automate.handler.FieldConfig;
import org.hy.automate.handler.ModelDefinition;
import org.hy.automate.utils.BeanUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OneToManyStatementGenerator extends AbstractGenerator{

    public OneToManyStatementGenerator() {
        super("oneToMany");
    }

    private Class<?> returnType;
    @Override
    protected MixedSqlNode getSqlNodes(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        List<FieldConfig> configs = modelDefinition.getFieldConfigs().stream()
                .filter(c->c.getField().isAnnotationPresent(OneToMany.class))
                .collect(Collectors.toList());

        if(!configs.isEmpty()){
            List<SqlNode> contents = new ArrayList<>();
            SqlNode sqlNode;
            for (FieldConfig config : configs) {
                Field field = config.getField();
                OneToMany oneToMany = field.getAnnotation(OneToMany.class);
                String foreignKey = oneToMany.foreignKey();
                Type type = field.getGenericType();
                ParameterizedType parameterizedType = (ParameterizedType) type;
                Class<?> clazz = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                returnType = clazz;
                String tableName = BeanUtils.getTableName(clazz);
                sqlNode = new TextSqlNode("SELECT * FROM ");
                contents.add(sqlNode);
                sqlNode = new TextSqlNode(tableName);
                contents.add(sqlNode);
                sqlNode = new WhereSqlNode(configuration,new TextSqlNode(foreignKey+" = #{param1}"));
                contents.add(sqlNode);
            }
            return new MixedSqlNode(contents);
        }
        return null;
    }
    @Override
    public MappedStatement generate(Configuration configuration, Class<?> mapperClass, ModelDefinition modelDefinition) {
        MixedSqlNode sqlNode = getSqlNodes(configuration, mapperClass, modelDefinition);
        if(sqlNode!=null){
            String namespace = mapperClass.getName();
            String statementId = namespace+"."+this.method;
            return register(configuration,sqlNode, SqlCommandType.SELECT,statementId,namespace,returnType,null);
        }
        return null;
    }
}

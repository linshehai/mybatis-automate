package org.hy.automate.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OneToMany {

    /**
     * 一的一方的key,默认是主键
     * @return
     */
    String column() default "";

    /**
     * 多对一的外键
     * @return
     */
    String foreignKey();

    /**
     * 延迟加载
     * @return
     */
    boolean lazy() default false;
}

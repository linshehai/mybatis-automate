package org.hy.automate.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    /**
     * 数据库中的列名
     * @return 返回列名
     */
    String name() default "";

    /**
     * 是否主键
     * @return 主键返回true，否则返回false
     */
    boolean primaryKey() default false;

    boolean exclude() default false;
}

package org.hy.automate.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ManyToOne {

    /**
     *
     * @return
     */
    String column() default "";

    /**
     * 被引用的key，默认主键
     * @return
     */
    String referencedKey() default "";
}

package org.hy.automate.plugin;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.hy.automate.base.Page;

import java.util.ArrayList;
import java.util.List;

@Intercepts(@Signature(type= Executor.class,method = "query",args = {
        MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class
}))
public class PageInterceptor implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        Object[] args = invocation.getArgs();
        MappedStatement mappedStatement = (MappedStatement) args[0];
        String id = mappedStatement.getId();

        if(id.lastIndexOf("selectPage")!=-1){
            Page<?> pageInfo = new Page<>();
            Executor executor = (Executor) invocation.getTarget();
            Configuration config = mappedStatement.getConfiguration();
            String countId = id.substring(0,id.lastIndexOf(".")+1)+"count";
            MappedStatement countSql = config.getMappedStatement(countId);
            List<Long> count = executor.query(countSql,args[1], (RowBounds) args[2],null);
            if(count!=null&&count.size()>0){
                Long total = count.get(0);
                pageInfo.setTotal(total);
            }

            List<?> list = (List<?>) invocation.proceed();

            pageInfo.setRecords(list);
            List<Page> pages = new ArrayList<>();
            pages.add(pageInfo);
            return pages;
        }
        return invocation.proceed();
    }
}

package org.hy.automate.base;

import java.util.List;

public class Page<T> {

    private List<T> records;

    private long total;

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<?> records) {
        this.records = (List<T>) records;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}

package org.hy.automate.base;

public class Pageable {

    private int pageNum;

    private int pageSize;

    public Pageable(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public static Pageable of(int pageNum, int pageSize){
        return new Pageable(pageNum, pageSize);
    }

    public int getPageNum() {
        return pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }
}

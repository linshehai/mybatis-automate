package org.hy.automate.base;

import org.apache.ibatis.annotations.Param;
import org.hy.automate.condition.Criteria;

import java.io.Serializable;
import java.util.List;

public interface BaseMapper<T> {

    int insert(T elem);

    int deleteById(Serializable id);

    int updateByIdSelective(T elem);

    List<T> select(@Param("criteria") Criteria<T> criteria);

    T selectById(Serializable id);

    Page<T> selectPage(@Param("criteria") Criteria<T> criteria, @Param("page") Pageable pageable);

    long count(Criteria<T> criteria);
}

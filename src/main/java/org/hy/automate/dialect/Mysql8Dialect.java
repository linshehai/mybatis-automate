package org.hy.automate.dialect;

import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;
import org.apache.ibatis.session.Configuration;

public class Mysql8Dialect implements Dialect {
    @Override
    public SqlNode getLimitSql(Configuration configuration) {
        return new TextSqlNode(" limit #{page.pageNum},#{page.pageSize} ");
    }
}

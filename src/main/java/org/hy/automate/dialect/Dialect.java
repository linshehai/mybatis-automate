package org.hy.automate.dialect;

import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.session.Configuration;

public interface Dialect {

    SqlNode getLimitSql(Configuration configuration);
}

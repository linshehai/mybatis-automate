package org.hy.automate.condition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Criteria<T>{

    private Map<String,Object> eqMap = new HashMap<>();

    private boolean orActive;
    private boolean andActive;

    private static final String AND = "and";
    private static final String OR = "or";

    private List<Conjunction> conjunctions;

    private Class<T> modelClass;

    public Criteria(Class<T> modelClass) {
        this.modelClass = modelClass;
        this.conjunctions = new ArrayList<>();
        this.andActive = true;
    }

    public static <T> Criteria<T> newCriteria(Class<T> modelClass){
        return new Criteria<>(modelClass);
    }

    /**
     * 相等条件判断
     * @param javaProperty 对应类的属性名称
     * @param value 值
     * @return
     */
    public Criteria<T> eq(String javaProperty,Object value){
        addCriteria(javaProperty,"=",value);
        return this;
    }

    private void addCriteria(String javaProperty, String operator, Object value) {
        if(orActive){
            if(andActive||this.conjunctions.size()<=0){
                this.conjunctions.add(new Conjunction(OR, javaProperty,operator, value));
                this.andActive = false;
            }else{
                this.conjunctions.get(this.conjunctions.size()-1).add(javaProperty,operator, value);
            }

        }else{
            if(orActive||this.conjunctions.size()<=0){
                this.conjunctions.add(new Conjunction(AND, javaProperty,operator, value));
            }else {
                this.conjunctions.get(this.conjunctions.size()-1).add(javaProperty,operator, value);
            }
        }
    }

    /**
     * like查询，默认没有通配符
     * @param javaProperty 对应类的属性名称
     * @param value 值
     * @return
     */
    public Criteria<T> like(String javaProperty,Object value){
        addCriteria(javaProperty,"like",value);
        return this;
    }
    /**
     * like查询，通配符在左边
     * @param javaProperty 对应类的属性名称
     * @param value 值
     * @return
     */
    public Criteria<T> likeLeft(String javaProperty,Object value){
        addCriteria(javaProperty,"like","%"+value);
        return this;
    }
    /**
     * like查询，通配符在右边
     * @param javaProperty 对应类的属性名称
     * @param value 值
     * @return
     */
    public Criteria<T> likeRight(String javaProperty,Object value){
        addCriteria(javaProperty,"like",value+"%");
        return this;
    }

    public Criteria<T> or(){
        this.orActive = true;
        return this;
    }
    public Criteria<T> and(){
        this.andActive = true;
        return this;
    }

    public List<Conjunction> getConjunctions() {
        return conjunctions;
    }

    public static class Conjunction{
        String predict;
        List<Expression> expressions;

        public Conjunction(String predict, String javaProperty,String operator, Object value) {
            this.predict = predict;
            this.expressions = new ArrayList<>();

            this.expressions.add(new Expression(javaProperty,operator,value));
        }
        void add(String javaProperty, String operator,Object value){
            this.expressions.add(new Expression(javaProperty,operator,value));
        }

        public String getPredict() {
            return predict;
        }

        public List<Expression> getExpressions() {
            return expressions;
        }
    }
    public static class Expression{

        public Expression(String leftValue,String operator, Object rightValue) {
            this.leftValue = leftValue;
            this.operator = operator;
            this.rightValue = rightValue;
        }
        String leftValue;
        String operator;
        Object rightValue;

        public String getLeftValue() {
            return leftValue;
        }

        public Object getRightValue() {
            return rightValue;
        }

        public String getOperator() {
            return operator;
        }
    }
}

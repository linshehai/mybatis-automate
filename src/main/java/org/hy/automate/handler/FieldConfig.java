package org.hy.automate.handler;

import java.lang.reflect.Field;

public class FieldConfig {

    private String javaProperty;

    private boolean primaryKey;

    private String column;

    private Field field;

    public FieldConfig(String javaProperty,String column,boolean primaryKey,Field field) {
        this.javaProperty = javaProperty;
        this.primaryKey = primaryKey;
        this.column = column;
        this.field = field;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getJavaProperty() {
        return javaProperty;
    }

    public void setJavaProperty(String javaProperty) {
        this.javaProperty = javaProperty;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Field getField() {
        return field;
    }
}

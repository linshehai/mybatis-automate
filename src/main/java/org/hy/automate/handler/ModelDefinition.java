package org.hy.automate.handler;

import org.apache.ibatis.mapping.ResultMapping;
import org.hy.automate.annotation.Table;
import org.hy.automate.utils.BeanUtils;
import org.hy.automate.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ModelDefinition {

    private Class<?> modelClass;

    private String tableName;

    private List<FieldConfig> fieldConfigs;

    public ModelDefinition(){

    }

    public ModelDefinition(Class<?> modelClass) {
        this.modelClass = modelClass;
        this.tableName = BeanUtils.getTableName(modelClass);
        this.fieldConfigs = new ArrayList<>();

    }

    public void addFieldConfig(FieldConfig fieldConfig){
        this.fieldConfigs.add(fieldConfig);
    }
    public Class<?> getModelClass() {
        return modelClass;
    }

    public String getColumnList() {
        List<String> cols = this.fieldConfigs.stream().filter(c->!StringUtils.isEmpty(c.getColumn())).map(fieldConfig -> fieldConfig.getColumn()).collect(Collectors.toList());
        return cols.toString().replaceAll("\\[|\\]","");
    }


    public String getTableName() {
        return tableName;
    }


    public String[] getFieldNames() {
        List<String> fs = this.fieldConfigs.stream().map(fieldConfig -> fieldConfig.getJavaProperty()).collect(Collectors.toList());
        return fs.toArray(new String[fs.size()]);
    }

    public List<FieldConfig> getFieldConfigs() {
        return fieldConfigs;
    }


}

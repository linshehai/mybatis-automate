package org.hy.automate.handler;

import org.apache.ibatis.mapping.ResultFlag;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.ResultMapping;
import org.apache.ibatis.session.Configuration;
import org.hy.automate.annotation.Column;
import org.hy.automate.annotation.ManyToOne;
import org.hy.automate.annotation.OneToMany;
import org.hy.automate.base.BaseMapper;
import org.hy.automate.generator.StatementGenerator;
import org.hy.automate.utils.BeanUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

public class GeneratorChain {


    private List<StatementGenerator> generators;

    public GeneratorChain(){
        generators = new ArrayList<>();
        ServiceLoader<StatementGenerator> serviceLoader = ServiceLoader.load(StatementGenerator.class);
        Iterator<StatementGenerator> it = serviceLoader.iterator();
        while (it.hasNext()){
            generators.add(it.next());
        }
    }

    public void handle(Configuration configuration){
        Collection<Class<?>> knownMappers= configuration.getMapperRegistry().getMappers();
        for (Class<?> knownMapper : knownMappers) {
            doGenerate(configuration, knownMapper);
        }
    }

    public void doGenerate(Configuration configuration, Class<?> knownMapper) {
        if(BaseMapper.class.isAssignableFrom(knownMapper) ){

            Type[] modelTypes = knownMapper.getGenericInterfaces();
            if(Proxy.isProxyClass(knownMapper)){
                modelTypes = ((Class)knownMapper.getGenericInterfaces()[0]).getGenericInterfaces();
            }
            Class<?> modelClass = null;
            if(modelTypes!=null&&modelTypes.length>0){
                ParameterizedType parameterizedType = (ParameterizedType)modelTypes[0];
                Type[] modelClasses = parameterizedType.getActualTypeArguments();
                modelClass = (Class<?>) modelClasses[0];
            }
            ModelDefinition modelDefinition = new ModelDefinition(modelClass);
            String namespace = knownMapper.getName();
            Class<?> spc = modelClass;
            List<ResultMapping> resultMappings = new ArrayList<>();
            List<ResultFlag> resultFlags = new ArrayList<>();

            //构建resultMapping
            while (spc!=null&&!spc.equals(Object.class)) {
                Field[] fields = spc.getDeclaredFields();
                List<Field> fs = Arrays.stream(fields).filter(f->{
                    Column c = f.getAnnotation(Column.class);
                    return c!=null&&c.primaryKey();
                }).collect(Collectors.toList());
                String fieldName, columnName;
                for (Field field : fields) {
                    fieldName = field.getName();
                    columnName = BeanUtils.getColumnName(fieldName);
                    Column column = field.getAnnotation(Column.class);
                    if (column != null) {
                        if(!column.exclude()){
                            columnName = column.name();
                            if(column.primaryKey()){
                                resultFlags.add(ResultFlag.ID);
                            }
                            modelDefinition.addFieldConfig(new FieldConfig(fieldName, columnName, column.primaryKey(),field));
                        }

                    } else {
                        columnName = (field.isAnnotationPresent(OneToMany.class)||field.isAnnotationPresent(ManyToOne.class))?"":columnName;
                        modelDefinition.addFieldConfig(new FieldConfig(fieldName, columnName, false,field));
                    }

                    ResultMapping.Builder builder = new ResultMapping.
                            Builder(configuration, fieldName, columnName, field.getType());
                    builder.flags(resultFlags);
                    //一对多配置
                    if(field.isAnnotationPresent(OneToMany.class)){
                        OneToMany oneToMany = field.getAnnotation(OneToMany.class);
                        builder.javaType(ArrayList.class);
                        builder.nestedQueryId(namespace+".oneToMany");
                        String col = oneToMany.column();
                        if(col==null||col.isEmpty()){
                            if(fs.size()>0){
                                col = fs.get(0).getAnnotation(Column.class).name();
                            }
                        }
                        builder.column(col);
                    }
                    //多对一配置
                    if(field.isAnnotationPresent(ManyToOne.class)){
                        ManyToOne manyToOne = field.getAnnotation(ManyToOne.class);
                        Class<?> clazz = field.getType();
                        builder.nestedQueryId(namespace+".manyToOne");
                        String col = manyToOne.column();
                        builder.javaType(clazz).column(col);

                    }
                    ResultMapping resultMapping = builder.build();
                    resultMappings.add(resultMapping);

                }
                spc = spc.getSuperclass();
            }
            String id = namespace+"-BaseResultMap";
            ResultMap rm = new ResultMap
                    .Builder(configuration,id,modelDefinition.getModelClass(),resultMappings)
                    .build();
            configuration.addResultMap(rm);
            for (StatementGenerator generator : generators) {
                generator.generate(configuration,knownMapper,modelDefinition);
            }
        }
    }

}

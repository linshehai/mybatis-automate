package org.hy.automate.factory;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.hy.automate.handler.GeneratorChain;

import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

public class AutomateSqlSessionFactoryBuilder extends SqlSessionFactoryBuilder {

    private GeneratorChain configurationHandler;

    public AutomateSqlSessionFactoryBuilder(){
        this.configurationHandler = new GeneratorChain();
    }
    @Override
    public SqlSessionFactory build(Reader reader) {
        return super.build(reader);
    }

    @Override
    public SqlSessionFactory build(Reader reader, String environment) {
        return super.build(reader, environment);
    }

    @Override
    public SqlSessionFactory build(Reader reader, Properties properties) {
        return super.build(reader, properties);
    }

    @Override
    public SqlSessionFactory build(Reader reader, String environment, Properties properties) {
        SqlSessionFactory sqlSessionFactory = super.build(reader, environment, properties);
        Configuration configuration = sqlSessionFactory.getConfiguration();
        configurationHandler.handle(configuration);
        return sqlSessionFactory;
    }

    @Override
    public SqlSessionFactory build(InputStream inputStream) {
        return super.build(inputStream);
    }

    @Override
    public SqlSessionFactory build(InputStream inputStream, String environment) {
        return super.build(inputStream, environment);
    }

    @Override
    public SqlSessionFactory build(InputStream inputStream, Properties properties) {
        return super.build(inputStream, properties);
    }

    @Override
    public SqlSessionFactory build(InputStream inputStream, String environment, Properties properties) {
        SqlSessionFactory sqlSessionFactory = super.build(inputStream, environment, properties);
        Configuration configuration = sqlSessionFactory.getConfiguration();
        configurationHandler.handle(configuration);
        return sqlSessionFactory;
    }

    @Override
    public SqlSessionFactory build(Configuration config) {
        return super.build(config);
    }
}

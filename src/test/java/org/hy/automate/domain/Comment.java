package org.hy.automate.domain;

import org.hy.automate.annotation.Column;
import org.hy.automate.annotation.ManyToOne;
import org.hy.automate.annotation.Table;

@Table("tb_comment")
public class Comment extends BaseModel{

    @Column(name = "id",primaryKey = true)
    private Integer id;

    private Integer blogId;

    private String  content;

    @ManyToOne(column = "blog_id")
    private Blog blog;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBlogId() {
        return blogId;
    }

    public void setBlogId(Integer blogId) {
        this.blogId = blogId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }
}

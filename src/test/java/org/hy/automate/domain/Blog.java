package org.hy.automate.domain;

import org.hy.automate.annotation.Column;
import org.hy.automate.annotation.OneToMany;
import org.hy.automate.annotation.Table;

import java.util.List;

@Table("blog")
public class Blog extends BaseModel{

    @Column(name = "id",primaryKey = true)
    private Integer id;

    @Column(name = "post_subject")
    private String subject;

    @OneToMany(foreignKey = "blog_id",lazy = true)
    private List<Comment> commentList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", commentList=" + commentList +
                '}';
    }
}

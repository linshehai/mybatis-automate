package org.hy.automate.base;

import junit.framework.TestCase;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.hy.automate.condition.Criteria;
import org.hy.automate.domain.Blog;
import org.hy.automate.factory.AutomateSqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class TestBlogMapper extends TestCase {

    private SqlSession session;

    private BlogMapper blogMapper;
    public void setUp(){
        InputStream is = TestBlogMapper.class.getClassLoader().getResourceAsStream("MinimalMapperConfig.xml");
        SqlSessionFactory sqlSessionFactory = new AutomateSqlSessionFactoryBuilder().build(is);
        session = sqlSessionFactory.openSession();
        Class<BlogMapper> mapperClass = BlogMapper.class;
        blogMapper = session.getMapper(mapperClass);
    }

    public void testSelectById(){
        Blog post = blogMapper.selectById(1);
        assert post!=null;
    }

    public void testSelectAllBlogs(){

        Criteria<Blog> criteria = Criteria.newCriteria(Blog.class);
//        criteria.eq("subject","qw").or().likeRight("subject","asqweq");
        List<Blog> postList = blogMapper.select(criteria);
        assert postList.size()>0;
    }

    public void testSelectPage(){
        Criteria<Blog> criteria = Criteria.newCriteria(Blog.class);
        criteria.eq("post_subject","qw").or().likeRight("post_subject","sdfsdfs");
        Page<Blog> postList = blogMapper.selectPage(criteria,Pageable.of(0,1));
        for (Blog blog:postList.getRecords()){
            System.out.println(blog);
        }
        System.out.println(postList);
    }
}

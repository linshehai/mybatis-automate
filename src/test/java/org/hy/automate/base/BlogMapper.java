package org.hy.automate.base;

import org.hy.automate.domain.Blog;

import java.util.List;

public interface BlogMapper extends BaseMapper<Blog>{

    List<Blog> selectAllBlogs();
}

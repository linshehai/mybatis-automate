package org.hy.automate.base;

import junit.framework.TestCase;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.hy.automate.condition.Criteria;
import org.hy.automate.domain.Comment;
import org.hy.automate.domain.Comment;
import org.hy.automate.factory.AutomateSqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class TestCommentMapper extends TestCase {

    private SqlSession session;

    private CommentMapper commentMapper;
    public void setUp(){
        InputStream is = TestCommentMapper.class.getClassLoader().getResourceAsStream("MinimalMapperConfig.xml");
        SqlSessionFactory sqlSessionFactory = new AutomateSqlSessionFactoryBuilder().build(is);
        session = sqlSessionFactory.openSession();
        commentMapper = session.getMapper(CommentMapper.class);
    }

    public void testSelectById(){
        Comment comment = commentMapper.selectById(1);
        assert comment!=null;
    }

    public void testSelectAllComments(){

        Criteria<Comment> criteria = Criteria.newCriteria(Comment.class);
//        criteria.eq("subject","qw").or().likeRight("subject","asqweq");
        List<Comment> postList = commentMapper.select(criteria);
        assert postList.size()>0;
    }

    public void testSelectPage(){
        Criteria<Comment> criteria = null;
        Page<Comment> commentPage = commentMapper.selectPage(criteria,Pageable.of(0,2));
        for (Comment Comment:commentPage.getRecords()){
            System.out.println(Comment);
        }
        System.out.println(commentPage);
    }
}
